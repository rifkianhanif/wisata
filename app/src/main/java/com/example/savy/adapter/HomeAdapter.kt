package com.example.savy.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.savy.R
import com.example.savy.databinding.WisataListBinding
import com.example.savy.model.Wisata

class HomeAdapter ( private val dataWisata : ArrayList<Wisata>) :
    RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    class ViewHolder(val binding : WisataListBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = WisataListBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataWisata.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.txtNama.text = dataWisata[position].nama
        holder.binding.txtLokasi.text = dataWisata[position].lokasi
        holder.binding.txtHarga.text = dataWisata[position].harga

        holder.binding.btnMore.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("nama", dataWisata[position].nama)
            bundle.putString("lokasi", dataWisata[position].lokasi)
            bundle.putString("harga", dataWisata[position].harga)
            bundle.putString("deskripsi", dataWisata[position].deskripsi)
            Navigation.findNavController(it).navigate(R.id.action_homeFragment_to_detailFragment, bundle)
        }
    }

    fun setData(newData: ArrayList<Wisata>) {
        dataWisata.clear()
        dataWisata.addAll(newData)
        notifyDataSetChanged()
    }


}