package com.example.savy.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.savy.adapter.HomeAdapter
import com.example.savy.databinding.FragmentHomeBinding
import com.example.savy.model.Wisata
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class HomeFragment : Fragment() {
    lateinit var binding: FragmentHomeBinding
    lateinit var adapter : HomeAdapter
    lateinit var databaseRef : DatabaseReference

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        adapter = HomeAdapter(ArrayList())
        binding.rvWisata.adapter = adapter
        binding.rvWisata.layoutManager = LinearLayoutManager(requireContext())

        databaseRef = FirebaseDatabase.getInstance().getReference("wisata")
        databaseRef.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val wisataList = ArrayList<Wisata>()
                for (snapshot in dataSnapshot.children) {
                    val wisata = snapshot.getValue(Wisata::class.java)
                    wisata?.let { wisataList.add(it) }
                    }
                    adapter.setData(wisataList)
                }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }

}