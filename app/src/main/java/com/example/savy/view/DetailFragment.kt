package com.example.savy.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.savy.R
import com.example.savy.databinding.FragmentDetailBinding

class DetailFragment : Fragment() {
    lateinit var binding: FragmentDetailBinding
    private lateinit var btn_book: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val nama = arguments?.getString("nama")
        val lokasi = arguments?.getString("lokasi")
        val harga = arguments?.getString("harga")
        val deskirpsi = arguments?.getString("deskripsi")

        binding.txtNama.text= nama
        binding.txtHarga.text = harga
        binding.txtDeskripsi.text = deskirpsi
        binding.txtLokasi.text = lokasi

    }
}