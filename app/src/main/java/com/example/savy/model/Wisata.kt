package com.example.savy.model

data class Wisata(val nama : String = "", val deskripsi : String = "", val img : String = "", val harga : String="", val lokasi : String="")
